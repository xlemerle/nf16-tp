#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tp4v2.c"
#define bool unsigned int
#define true 1
#define false 0

int main()
{
    bool quitter = false;
    int choix;
    char* n1 = "lemerle";
    char* p1 = "xavier";
    char nom[30];
    char prenom[30];
    char motif[100];
    char date[30];
    int niveau;
    Patient* P1 = creerPatient(n1, p1);
    Parbre arbre = P1;
    while(quitter==false){
        printf("\n\n-----------------------------------------");
        printf("\nVeuillez choisir le numero");
        printf("\n1. Ajouter un patient");
        printf("\n2. Ajouter une consultation a un patient");
        printf("\n3. Afficher une fiche médicale");
        printf("\n4. Afficher la liste des patients");
        printf("\n5. Supprimer un patient");
        printf("\n6. Copier ou mettre a jour la liste des patients");
        printf("\n7. Quitter");
        printf("\n-----------------------------------------\n");
        scanf("%d",&choix);
        switch(choix){
        case 1:
            printf("\nEntrer le nom du patient : ");
            scanf("%s", nom);
            printf("\nEntrer le prenom du patient : ");
            scanf("%s", prenom);
            if(arbre == NULL){
                printf("arbre nul");
                inserer_patient(&arbre, nom, prenom);
            }
            inserer_patient(&arbre, nom, prenom);
            break;
        case 2:
            printf("\nEntrer le nom du patient : ");
            scanf("%s", nom);
            printf("\nAjouter la date de la consultation : ");
            scanf("%s", date);
            printf("\nAjouter le motif de la consultation : ");
            scanf("%s", motif);
            printf("\nAjouter le niveau de la consultation : ");
            scanf("%d", &niveau);
            ajouter_consultation(&arbre, nom, date, motif, niveau);
            break;
        case 3:
            printf("\nEntrer le nom du patient : ");
            scanf("%s", nom);
            afficher_fiche(&arbre, nom);
            break;
        case 4:
            printf("\nListe de tout les patients : \n");
            afficher_patients(&arbre);
            break;
        case 5:
            printf("\nEntrer le nom du patient : ");
            scanf("%s", nom);
            break;
        case 6:
            break;
        case 7:
            quitter=true;
            break;
        }
    }
    afficher_un_patient(arbre);
    //afficher_un_patient(arbre->fils_gauche);
    afficher_un_patient(arbre->fils_droit);
    return 0;
}
