#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tp4.h"
#define M 20

patient * creerpatient(char *nm,char *pr){
     patient *nouveaupatient=(patient*)malloc(sizeof(patient));
     nouveaupatient->nom=(char*)malloc(M*sizeof(char));
     nouveaupatient->prenom=(char*)malloc(M*sizeof(char));
     strcpy(nouveaupatient->nom,nm);
     strcpy(nouveaupatient->prenom,pr);
     nouveaupatient->fils_droit=NULL;
     nouveaupatient->fils_gauche=NULL;
     nouveaupatient->nbrconsult=0;
     nouveaupatient->listconsult=NULL;
     return nouveaupatient;
}

void inserer_patient(parbre *abr,char *nm,char *pr){               //Je essaie de utilise parbre p   p=*abr
        if((*abr)==NULL)                                           // mais je trouve ca ne marche pas. Je sais pas pourqoui
            (*abr)=creerpatient(nm,pr);
        else if(strcmp(nm,(*abr)->nom)>0){                         //A->Z gauche->droit
            inserer_patient(&((*abr)->fils_droit),nm,pr);
        }
        else if(strcmp(nm,(*abr)->nom)<0){
            inserer_patient(&((*abr)->fils_gauche),nm,pr);
        }
}

patient *rechercher_patient(parbre *abr, char *nm){
    if((*abr)==NULL){
        printf("Personne!\n");
        return NULL;
    }
    if(strcmp(nm,(*abr)->nom)==0){
        printf("%s", (*abr)->nom);
        return (*abr);
    }
    else if (strcmp(nm,(*abr)->nom)<0)
        return rechercher_patient(&(*abr)->fils_gauche,nm);

    else
        return rechercher_patient(&(*abr)->fils_droit,nm);
}

void afficher_fiche(parbre *abr,char *nm){
    parbre p;
    p=rechercher_patient(abr,nm);
    int i;
    if(p!=NULL){
        if(p->listconsult!=NULL){                                              //operation de list-chaine
            for(i=1;i<=(p->nbrconsult);i++){
                printf("%d consult: date: %s, motif: %s, niveauurgent: %d.\n",i,p->listconsult->date,p->listconsult->motif,p->listconsult->niveauUrg);
                p->listconsult=p->listconsult->suivant;
            }
        }
        else
            printf("Aucun enregistrement!\n");
    }
    else
        printf("Name error,il n'y a pas de ce patient!\n");
}

void afficher_patients(parbre *abr){
    if((*abr)==NULL)
        return;
    printf(" %s %s\n",(*abr)->nom,(*abr)->prenom);
    afficher_patients(&((*abr)->fils_gauche));
    afficher_patients(&((*abr)->fils_droit));

}

consultation *creerconsult(char *date,char *motif,int nivu){
    consultation *nouvelle=malloc(sizeof(consultation));
    nouvelle->date=malloc(M*sizeof(char));
    nouvelle->motif=malloc(M*sizeof(char));
    strcpy(nouvelle->date,date);
    strcpy(nouvelle->motif,motif);
    nouvelle->niveauUrg=nivu;
    nouvelle->suivant=NULL;
    return nouvelle;
}

void ajouter_consultation(parbre *abr,char *nm,char *date,char *motif, int nivu){
    parbre p;
    p=rechercher_patient(abr,nm);
    consultation *c=creerconsult(date,motif,nivu);
    consultation *cp;
    if(p!=NULL){
        if(p->nbrconsult==0)
            p->listconsult=c;
        else{
            cp=p->listconsult;
            for(int i=1;i<(p->nbrconsult);i++)
                cp=cp->suivant;
            cp->suivant=c;
        }
        p->nbrconsult++;
    }
    else
        printf("Name error,il n'y a pas ce patient!\n");
}

void freepatient(parbre *abr){                      //supprimer d'abord listconsuit, ensuite consultation, enfin p
    consultation *c = (*abr)->listconsult;
    consultation *ct;
    while(c!=NULL){
        ct=c;
        free(ct->date);
        free(ct->motif);
        free(ct);
        c=c->suivant;
    }
    free((*abr)->nom);
    free((*abr)->prenom);
    free((*abr));
}

patient * pere_patient(parbre *abr, patient *p){                //retourne le père d'un patient donné
    while((*abr)->fils_droit!=p && (*abr)->fils_gauche!=p){
        if(strcmp(p->nom,(*abr)->nom)>0){
            (*abr)=(*abr)->fils_droit;
            printf("fils droit : %s\n", (*abr)->nom);
        }
        if(strcmp(p->nom,(*abr)->nom)<0){
            (*abr)=(*abr)->fils_gauche;
            printf("fils gauche : %s\n", (*abr)->nom);

        }
    }
    printf("Resultat final : %s\n", (*abr)->nom);
    return (*abr);
}

//ne tiens pas compte de cette fonction, c'était pour tester quelquechose
patient * pere_patient2(parbre *abr, patient *p){                //retourne le père d'un patient donné
    parbre arbre = *abr;                                         //copie pour ne pas modifier l'abre
    while(arbre->fils_droit!=p && arbre->fils_gauche!=p){
        if(strcmp(p->nom, arbre->nom)>0){
            arbre=arbre->fils_droit;
            printf("fils droit : %s\n", arbre->nom);
        }
        if(strcmp(p->nom,arbre->nom)<0){
            arbre=arbre->fils_gauche;
            printf("fils gauche : %s\n", arbre->nom);
        }
}
    printf("Resultat final : %s\n", arbre->nom);
    return arbre;
}

patient * min_patient(patient *p){                         //retourne le patient avec plus petit nom
    while (p->fils_gauche!=NULL)
        p=p->fils_gauche;
    return p;
}

void supprimer_patient(parbre *abr,char *nm){
    parbre arbre = *abr;
    patient* p=rechercher_patient(&arbre,nm);
    if (arbre==NULL) return;
    if(p->fils_droit==NULL && p->fils_gauche==NULL){               //cas où le patient n'a pas de fils
        if(arbre->fils_droit==NULL && arbre->fils_gauche==NULL){ //s'il n'y a que la racine
            freepatient(&arbre);
            arbre=NULL;
            return;
        }
        patient* p_pere=pere_patient(&arbre,p);
        if(p_pere->fils_gauche==p){
            printf("fils gauche du pere est supp\n");
            p_pere->fils_gauche=NULL;
        }
        else{
            p_pere->fils_droit=NULL;
            printf("fils droit du pere est supp\n");
        }
        freepatient(&p);
        return;
    }

    if(p->fils_gauche!=NULL && p->fils_droit!=NULL){            //cas où le patient à deux fils
        patient* pmin=min_patient(p->fils_droit);               //on remplace par le plus petit nom suivant le patient
        char* nom=(char*)malloc(M*sizeof(char));
        strcpy(nom,pmin->nom);
        p->nbrconsult=pmin->nbrconsult;
        consultation* c=p->listconsult;
        for(int i=0;i<p->nbrconsult;i++){
            ajouter_consultation(&arbre,p->nom,c->date,c->motif,c->niveauUrg);
            c=c->suivant;
        }
        supprimer_patient(&arbre,pmin->nom);                   //suppression de pmin qui n'a pas de fils
        strcpy(p->nom,nom);
        free(nom);
        return;
    }

    if(p->fils_droit!=NULL || p->fils_gauche!=NULL){        //cas où le patient n'a qu'un fils
        if(arbre==p){                                        //si la racine n'a qu'un fils
            if(p->fils_gauche!=NULL){
                arbre=p->fils_gauche;
            }
            else{
                arbre=p->fils_droit;
            }
            freepatient(&p);
            return;
        }
        patient* p_pere=pere_patient(&arbre,p);
        if(p->fils_gauche!=NULL && p->fils_droit==NULL){
            if(p_pere->fils_gauche==p){
                p_pere->fils_gauche=p->fils_gauche;
            }
            else {
                p_pere->fils_droit=p->fils_gauche;
            }
            freepatient(&p);
        return;
        }

        if(p->fils_gauche==NULL && p->fils_droit!=NULL){
            if(p_pere->fils_gauche==p){
                p_pere->fils_gauche=p->fils_droit;
            }
            else{
                p_pere->fils_droit=p->fils_droit;
            }
            freepatient(&p);
            return;
        }
    }
}

void maj(parbre *abr,parbre *abr2){
    while((*abr2)!=NULL){
        supprimer_patient(&(*abr2),(*abr2)->nom);
    }
    if((*abr)!=NULL)
        inserer_patient(abr2,(*abr)->nom,(*abr)->prenom);
    if((*abr)->fils_gauche!=NULL)
        maj(&((*abr)->fils_gauche),abr2);
    if((*abr)->fils_droit!=NULL)
        maj(&((*abr)->fils_droit),abr2);
}
void freeabr(parbre* abr){

        if((*abr)!=NULL)
            supprimer_patient(&(*abr),(*abr)->nom);
        if((*abr)->fils_droit!=NULL)
            supprimer_patient(&((*abr)->fils_droit),(*abr)->fils_droit->nom);
        if((*abr)->fils_gauche!=NULL)
            supprimer_patient(&((*abr)->fils_gauche),(*abr)->fils_gauche->nom);

}
